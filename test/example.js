"use strict";
const fs = require("fs");
const express = require('express');

var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const src_1 = require("../src");
const { response } = require("express");
const ADD_ROSPEC = __importStar(require("./ADD_ROSPEC.json"));
const ADD_ROSPEC_DATA = ADD_ROSPEC.data;
// create a client
//const reader = new LLRPClient({ host: "192.168.7.2" });
const reader = new src_1.LLRPClient({host: "169.254.1.1"});

reader.on("RO_ACCESS_REPORT", msg => {

        let tagReportDataList = msg.getTagReportData();
        var newObject = { rfid: []  }; 


            if (!Array.isArray(tagReportDataList)) {
                tagReportDataList = [tagReportDataList];
                }
            for (let tagReportData of tagReportDataList) {
                    let epc = tagReportData.getEPCParameter();
                    var rfidArray = epc.getEPC();
                    console.log(rfidArray);
                    newObject.rfid.push(rfidArray);
            }
                console.log(newObject);
                var jsondata = JSON.stringify(newObject);
                fs.writeFileSync("Tags.json",jsondata,function(err){
                    if(err){console.log(err)}
                });
                process.exit(1);
                
});

reader.on("READER_EVENT_NOTIFICATION", msg => {
    // handle reader event notification messages here
});
reader.on("error", err => {
    // handle errors
});
reader.on("connect", () => {
    console.log("connected");
});
reader.on("disconnect", () => {
    console.log("disconnected");
});
/** Main */
(async () => {
        // connect to reader
        await reader.connect();
        // wait for connection confirmation
        await checkConnectionStatus();
        // start scanning for RFID tags
        await startReaderOperation();
        setTimeout(() => {
            desconectar();
        }, 1000);
})();
/**
 * wait for a READER_EVENT_NOTIFICATION message
 */
const checkConnectionStatus = async () => {
    let msg = await reader.recv(7000);
    if (!(msg instanceof src_1.LLRPCore.READER_EVENT_NOTIFICATION)) {
        throw new Error(`connection status check failed - unexpected message ${msg.getName()}`);
    }
    const status = msg.getReaderEventNotificationData().getConnectionAttemptEvent()?.getStatus();
    if (status != "Success") {
        throw new Error(`connection status check failed ${status}`);
    }
    return;
};
/**
 * perform all operations necessary to start the reader
 */
const startReaderOperation = async () => {
    // delete all existing ROSpecs (if any)
    await reader.transact(new src_1.LLRPCore.DELETE_ROSPEC({
        data: { ROSpecID: 0 /** all */ }
    }));
    // factory reset
    await reader.transact(new src_1.LLRPCore.SET_READER_CONFIG({
        data: { ResetToFactoryDefault: true }
    }));
    // add ROSpec defined in "./ADD_ROSPEC.json"
    await reader.transact(new src_1.LLRPCore.ADD_ROSPEC({
        data: ADD_ROSPEC_DATA
    }));
    // enable ROSpec
    await reader.transact(new src_1.LLRPCore.ENABLE_ROSPEC({
        data: { ROSpecID: ADD_ROSPEC_DATA.ROSpec.ROSpecID }
    }));
    // start ROSpec
    await reader.transact(new src_1.LLRPCore.START_ROSPEC({
        data: { ROSpecID: ADD_ROSPEC_DATA.ROSpec.ROSpecID }
    }));
};


let desconectar = ( async () => {
    // request termination
    const rsp = await reader.transact(new src_1.LLRPCore.CLOSE_CONNECTION);
    if (rsp instanceof src_1.LLRPCore.CLOSE_CONNECTION_RESPONSE) {
        const status = rsp.getLLRPStatus();
        console.log(`${status.getErrorDescription()} - ${status.getStatusCode()}`);
    }
    // make sure it's disconnected
    if (reader.socketWritable)
        await reader.disconnect();
});
//# sourceMappingURL=example.js.map

